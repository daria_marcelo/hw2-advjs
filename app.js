// Загалом, конструкція try...catch доречна в будь-якій ситуації, коли виникнення помилки може призвести до зупинки роботи програми або неочікуваних наслідків. Під час роботи зі зв'язками з базою даних, де можуть виникати помилки підключення або запитів до бази даних, під час роботи з файлами, де можуть виникати помилки під час читання або запису файлів, під час виконання операцій, що можуть викликати помилки, таких як математичні операції або робота з даними користувача, під час виконання коду, де потрібно забезпечити продовження роботи програми, навіть якщо виникнуть помилки.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function createList (books) {
    const root = document.getElementById("root");
    const ul = document.createElement("ul");

    books.forEach(book => {
        if (typeof book.author === "undefined" || typeof book.name === "undefined" || typeof book.price === "undefined") {
            console.error(`Помилка: в об'єкті наявні не всі властивості: ${JSON.stringify(book)}`);
            return;
        }
        const li = document.createElement("li");
        li.innerHTML = `Автор: ${book.author}<br>Назва: ${book.name}<br>Ціна: ${book.price}`;
        ul.append(li);
    });

    root.append(ul);
}

try {

    createList(books);

} catch (error) {

    console.log(`Помилка  ${error.name}: ${error.message}`);
}
